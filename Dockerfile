FROM php:7.4-apache

RUN echo "<?php phpinfo(); ?>" > /var/www/html/index.php
RUN echo "ServerName apps.kafinea.com" >> /etc/apache2/apache2.conf
RUN echo "UseCanonicalName On" >> /etc/apache2/apache2.conf
RUN echo "RequestHeader set Host apps.kafinea.com" >> /etc/apache2/apache2.conf
RUN mkdir /var/www/html/kfnqt8n476wgq28 && cp -f /var/www/html/index.php /var/www/html/kfnqt8n476wgq28/

RUN apt update && apt -y install vim

RUN a2enmod rewrite
RUN a2enmod headers

EXPOSE 80

CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
